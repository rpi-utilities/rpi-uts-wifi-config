#!/bin/bash -e
# A script to configure WiFi connectivity for the University of Technology Sydney WPA2 enterprise network

logger() {
    # Get arguments
    local msg=$1
    local msgType=$2  # Optional message type

    # ANSI colour escape codes
    local yellow='\033[1;33m'
    local red='\033[1;31m'
    local cyan='\033[1;36m'
    local normal='\033[0m'
    local green='\033[1;32m'

    # Determine message type
    case $msgType in
        warning)
            msgPrefix="${yellow}[WARNING]${normal}"
            ;;
        error)
            msgPrefix="${red}[ERROR]${normal}"
            ;;
        success)
            msgPrefix="${green}[SUCCESS]${normal}"
            ;;
        *)
            msgPrefix="${cyan}[INFO]${normal}"
            ;;
    esac

    # Output log message
    echo -e "$msgPrefix $msg"
}

usage() {
    logger "Unexpected number of arguments" "error"
    echo "Usage: $(basename "$0") [StudentID]"
    exit 1
}

getInput() {
    read -p "Please enter your ${1}: " -r $2
    echo $REPLY
}

confirmation() {
    read -p "Do you want to continue? [Y/n] " -n 1 -r
    echo  # Move to a new line
    [[ $REPLY =~ ^[Yy]$ ]] || exit 0
}

backup() {
    local file=$1
    local backupTimestamp=$2

    logger "Backing up $file"
    cp $file ${file}.bak${backupTimestamp}
    logger "${file}.bak${backupTimestamp} created"
}

configureWpaSupplicantConf() {
    local backupTimestamp=$1
    local studentId=$2
    local passwordHash=$3
    local file=/etc/wpa_supplicant/wpa_supplicant.conf

    backup $file $backupTimestamp

    logger "Replacing $file contents"

    cat > $file << EOF
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=AU
network={
    ssid="UTS-WiFi"
    scan_ssid=0
    key_mgmt=WPA-EAP
    identity="${studentId}"
    password=hash:${passwordHash}
    eap=PEAP
    phase2="auth=MSCHAPV2"
}
EOF
}

lockWlanDriver () {
    local backupTimestamp=$1
    local file=/etc/dhcpcd.conf

    backup $file $backupTimestamp

    logger "Modifying $file"

    # This fix is required in Raspbian Buster, as per https://www.raspberrypi.org/forums/viewtopic.php?t=247310
        cat >> $file << EOF

# Fix for WPA2 enterprise authentication issue with nl80211 driver
# Refer to https://www.raspberrypi.org/forums/viewtopic.php?t=247310
interface wlan0
env ifwireless=1
env wpa_supplicant_driver=wext,nl80211
EOF
}

# Check usage and get arguments
expectedArgs=1
if [[ $# -gt $expectedArgs ]]
then
    usage
elif [[ $# -eq $expectedArgs ]]
then
    logger "Parsing command argument as student ID"
    studentId=$1
else
    studentId=$(getInput "student ID")
fi
passwordHash=$(echo -n $(getInput "student password" "-s") | iconv -t utf-16le | openssl md4 | sed 's/^.*= //')
echo  # Move to a new line
logger "Password hashed"

# Display disclaimer
logger "This script will overwrite and modify the following files:\n\t- /etc/dhcpcd.conf\n\t- /etc/wpa_supplicant/wpa_supplicant.conf" "warning"

# Display backup details
backupTimestamp=$(date +%s)
logger "The following backup of each file will be made:\n\t- /etc/dhcpcd.conf.bak${backupTimestamp}\n\t- /etc/wpa_supplicant/wpa_supplicant.conf.bak${backupTimestamp}"

# Get user confirmation
confirmation

# Apply configuration
configureWpaSupplicantConf $backupTimestamp $studentId $passwordHash
lockWlanDriver $backupTimestamp

logger "UTS WiFi configured - now reboot!" "success"
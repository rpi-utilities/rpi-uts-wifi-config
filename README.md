# RPI UTS WiFi Enterprise Config

A simple script to configure a RaspberryPi for the University of Sydney WPA2 enterprise network.
This script will overrite and modify the following files:

- `/etc/wpa_supplicant/wpa_supplicant.conf`
- `/etc/dhcpcd.conf`

**Note:** Due to a bug in Raspbain Buster at the time of writing this, additional config to specify the wlan0 driver has to be added to `/etc/dhcpcd.conf`.
More details on this issue can be found at the following [thread](https://www.raspberrypi.org/forums/viewtopic.php?t=247310).

## Usage

To use the script simply copy it to the RaspberryPi and execute it.

```bash
sudo ./configure-uts-wifi.sh
```

Alternatively, you can also optionally specify your student ID as an argument.

```bash
sudo ./configure-uts-wifi.sh 12345678
```

If you have an execution permission issue remember to make sure the script you copied is executable.

```bash
chmod +x ./configure-uts-wifi.sh
```
